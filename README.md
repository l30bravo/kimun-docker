# Kimun Doker

## Course
* [docker-essential](https://www.udemy.com/course/docker-essentials/)
* [docker-course pelao nerd](https://www.youtube.com/playlist?list=PLqRCtm0kbeHAep1hc7yW-EZQoAJqSTgD-)


## Commands
* Documentation: https://docs.docker.com/engine/reference/commandline/docker/
* Extra: https://clouding.io/hc/es/articles/360010283060-Trabajando-con-im%C3%A1genes-en-Docker

* Pull Image
```bash
docker pull {IMAGE-NAME}
```
for example
```bash
docker pull alpine
```

* List Images
```bash
docker images
```

* Run image:
```bash
docker run {IMG-NAME}||{IMG-ID}
```

for example:
```bash
docker run alpine
```

* run a commnd
```bash
docker run {image} {bash-command}
```
* docker stop
```bash
docker stop {ps-id}
```


* run and login example
```bash
docker run -it ubuntu /bin/bash
```

* list of conteiners running
```bash
docker ps
```

* list only the last ones
```bash
docker ps -a | head
```

* login into one running conteiner
```bash
docker exec -it {running-id} sh
```

* Get the history log
```bash
docker image history {image-id}
```

## How to create new images
Example:

* Start a new one
```bash
docker run -it ubuntu /bin/bash
```

* Modify one
into the docker instance:
```bash
apt update
apt install figlet
figlet holiwi
exit
```

* coomit one
```bash
docker commit {PS-ID} 
```

* get the new id image
```bash
docker images
```


* tag the image
```bash
docker image tag {IMAGE-ID} {NAME}:{VERSION}
```

for example

```bash
docker image tag fc303bd0e222 midocker:1.0
```

* run de image with the app installed

```bash
docker run {nombre} {app} {input}
```

for example:

```bash
docker run midocker figlet wena-shoro
```
### from docker file

```bash
touch Dockerfile
```
into this file write:

```bash
FROM ubuntu

RUN apt update

RUN apt install figlet -y
```

* build image
Docker build por defecto va a leer el Dockerfile, entonces necesitas declarar el nombre de esta nueva imagen y la versión
```bash
docker build -t midocker:1.1
```

* check the new image

```bash
docker image ls | head
```

## Ports and volumes
* Example with ngnix
```bash
docker run -d nginx:1.15.7
```
`-d` leave the container running in background

* get into
```bash
docker exec -it 17eb76146da9 bash
```
and run:
```bash
apt install procps curl
```

* volumes
This steps are to mount a partition / folder into the container.

```bash
docker run -v /home/user/my/path/kimun-docker/index.html:/usr/share/nginx/html/index.html:ro -d nginx:1.15.7
```
* Expouse ports
Redirigir el tráfico del contenedor desde el puerto 80 al 8080 en la computadora

```bash
docker run -v /home/user/my/path/kimun-docker/index.html:/usr/share/nginx/html/index.html:ro -d -p 8080:80  nginx:1.15.7
```
This also is very usefull to persis data, rember when de docker instance shot down, all information is goind to die fi you dont run (start) again the same docker instance.
one way to persis data with docker is mount one folder where you are going to store dat data, and when you run again other docker instanace, you can mount again that folder.

## Adding environment variables into docker instances
```bash
docker run -e {env-var}={env-value} {image}
```

other way to do the same think is Docker Compose

## Docker Compose
Into the docker-compose-start-example.yaml file, you can define the services with their images, environment variables, volumes among other things.
In link level you can put the name of the image from other docker and docker internally is going to resolve the ip address
to run this yaml:
```bash
docker-compose up -d
```
to validate the execution of this docker-compose
```bash
docker ps
```
or
```bash
docker-compose ps
```

# Service
```bash
sudo service docker restart
```

# Networking
you can review this configuration in `docker-compose-network-example.yaml`

# Multi Stage Builds
Basically this is a "new" feature to declare two or more images into one dockerfile
You can see the example in the folder named `multi-sage-builds`
The copy `--from = 0` is the magic to copy the app that was packed in GO's docker, to alpine's docker

```bash
docker build -t multi-stage-build/example:latest
```

## Instalation
* Source: https://linuxhint.com/install_configure_docker_ubuntu/


## Errors
### How to fix docker: Got permission denied while trying to connect to the Docker daemon socket
* Solution: https://www.digitalocean.com/community/questions/how-to-fix-docker-got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket
, the Error:
```bash
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json: dial unix /var/run/docker.sock: connect: permission denied
```

### Other links
* https://github.com/pablokbs/peladonerd
